/*! \file  Delay_ms.c
 *
 *  \brief Delay for a specified number of milliseconds
 *
 * Loop for as long as requested (in milliseconds) in the
 * passed parameter.
 *
 *  \author jjmcd
 *  \date January 28, 2015, 10:44 AM
 *
 * Software License Agreement
 * Copyright (c) 2014 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>

/*! Delay_ms - Delay for LCD */
/*! Delay for a specified number of milliseconds
 *  This routine wastes time for the number of milliseconds passed in
 *  as a parameter.
 *
 *  The function executes 534 nop instructions (plus loop overhead)
 *  for as many times as requested in the passed parameter.
 *
 *  \param n - number of milliseconds to delay
 *  \return void
 */
void Delay_ms( unsigned int n )
{
  long i,j;

  for ( i=0; i<n; i++ )
    for ( j=0; j<534; j++ )
      Nop();
  return;
}

