/*! \file  LCDletter.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date January 28, 2015, 10:57 AM
 *
 * Software License Agreement
 * Copyright (c) 2014 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>
#include "LCDinternal.h"


//! Send a character to the LCD
/*!  This routine simply sends a data byte to the LCD.  The
 *   register select pin is set to 1 notifying the LCD that the
 *   byte is to be used as a displayed character.
 *
 * \par data char - Character to send to the LCD
 * \return none
 */
void LCDletter( char data )
{
    LCDbusy();
    LCD_RS = 1; // assert register select to 1
    LCDsend( data );
    LCD_RS = 1; // negate register select to 0
}

