/*! \file  LCDputs.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date January 28, 2015, 10:58 AM
 *
 * Software License Agreement
 * Copyright (c) 2014 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "LCD.h"


//! Send a string to the LCD
/*! Sends a null-terminated string to the LCD
 *
 * \par p char * - pointer to string to be displayed
 * \return none
 */
void LCDputs( char *p )
{
    while (*p)
    {
        LCDletter(*p);
        p++;
    }
}
