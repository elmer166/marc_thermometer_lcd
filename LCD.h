/*! \file LCD.h
 *
 *  \brief LCD external definitions
 */

/******	LCD FUNCTION PROTOYPES ******/

//! Initialize the LCD
void LCDinit( void );
//! Send a command to the LCD
void LCDcommand( char cmd );
//! Send a character to the LCD
void LCDletter( char data );
//! Send a string to the LCD
void LCDputs( char * );
//! Delay for a specified number of milliseconds
void Delay_ms( unsigned int );


/*****	LCD COMMAND MACROS  *****/

//! Move the LCD cursor to the right
#define LCDright()      LCDcommand( 0x14 )
//! Move the LCD cursor to the left
#define LCDleft()       LCDcommand( 0x10 )
//! Shift the LCD display
#define LCDshift()      LCDcommand( 0x1C )
//! Clear the LCD display and home cursor
#define LCDclear()     { LCDcommand( 0x01 ); LCDcommand( 0x02 ); }
//! Set the LCD cursor to home
#define LCDhome()       LCDcommand( 0x02 )
//! Position the LCD cursor to the second line
#define LCDline2()      LCDcommand( 0xC0 )
//! Set the LCD cursor position
#define LCDposition(a)  LCDcommand( 0x80 + ( a & 0x7f) )
