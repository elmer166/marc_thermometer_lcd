/*! \file  LCDpulseEnableBit.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date January 28, 2015, 10:45 AM
 *
 * Software License Agreement
 * Copyright (c) 2014 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>
#include "LCDinternal.h"

//! Toggle the LCD enable bit
/*! Each LCD command is strobed into the device by raising the
 * enable bit for at least 40 microseconds.  This routine
 * provides this function to the other functions in the
 * library.
 *
 * \par none
 * \return none
 */
void LCDpulseEnableBit( void )
{
  // Give data a chance to stabilize
  Nop();

  LCD_ENABLE = 1;

  // A little time to see the enable
  Nop();
  Nop();

  LCD_ENABLE = 0; // toggle E signal
}

