/*! \file  LCDbusy.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date January 28, 2015, 10:49 AM
 *
 * Software License Agreement
 * Copyright (c) 2014 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include "LCD.h"

//! Wait while LCD is busy
/*!  Dummied up with a 1ms delay
 *
 * \par none
 * \returns none
 */
void LCDbusy( void )
{
    Delay_ms(1);
}

