/*! \file  LCDcommand.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date January 28, 2015, 10:53 AM
 *
 * Software License Agreement
 * Copyright (c) 2014 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>
#include "LCDinternal.h"
#include "LCD.h"

//! Send a command to the LCD
/*!  This routine simple sends a data byte to the LCD.  The
 *   register select pin is set to 0 notifying the LCD that the
 *   byte is to be interpreted as a command.
 *
 * \param cmd char - Command byte to send to LCD
 * \returns none
 *
 *   \todo This routine delays 400us after sending the byte.  Instead
 *   the LCD busy flag should be checked before sending the byte.
 *   All routines using delays, however, must follow this protocol
 */
void LCDcommand( char cmd )
{
    LCDbusy();
    LCD_RS = 0; // assert register select to 1
    LCDsend( cmd );
    Delay_ms(1);
    LCD_RS = 1; // negate register select to 0
}

