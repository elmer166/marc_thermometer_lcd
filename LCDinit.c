/*! \file  LCDinit.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date January 28, 2015, 10:55 AM
 *
 * Software License Agreement
 * Copyright (c) 2014 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>
#include "LCDinternal.h"
#include "LCD.h"


//! Initialize the LCD
/*!  LCDinit() first delays 15ms to allow the LCD internals to finish
 *   responding to power on.  This is not always necessary, but typically
 *   only happens once in a program.  The LCD initialization sequence is
 *   then sent, setting the LCD to  bit data.
 *
 *   LCD options are then sent, LCD 2 line, 5x7 font,  entry mode to
 *   not shift, display on, cursor off.
 *
 * \par none
 * \return none
 */
void LCDinit( void)
{
    // 15mS delay after Vdd reaches nnVdc before proceeding with LCD initialization
    // not always required and is based on system Vdd rise rate
    Delay_ms(15);

    /* set initial states for the data and control pins */
    LCD_DATA &= 0x0FFF;
    LCD_RW = 0; // R/W state set low
    LCD_RS = 0; // RS state set low
    LCD_ENABLE = 0; // E state set low

    /* set data and control pins to outputs */
    LCD_DATATRIS &= 0x0FFF;
    LCD_RW_TRIS = 0; // RW pin set as output
    LCD_RS_TRIS = 0; // RS pin set as output
    LCD_ENABLE_TRIS = 0; // E pin set as output

    /* 1st LCD initialization sequence */
    LCDcommand(0x33);
    Delay_ms(5);

    /* 2nd LCD initialization sequence */
    LCDcommand(0x32);
    Delay_ms(5);

    // Establish the LCD options
    LCDcommand(0x28); // function set
    LCDcommand(0x0C); // Display on/off control, cursor blink off (0x0C)
    LCDcommand(0x06); // entry mode set (0x06)
}
